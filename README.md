# tuna-asyncio-with-llama

## Introducing Tuna - A Tool for Rapidly Generating Synthetic Fine-Tuning datasets
this is a fork from https://blog.langchain.dev/introducing-tuna-a-tool-for-rapidly-generating-synthetic-fine-tuning-datasets/

## Brief Overview
tuna-asyncio-with-llama is a no-code tool for quickly generating LLM fine-tuning datasets from scratch. This enables anyone to create high-quality training data for fine-tuning large language models like the LLaMas. There a Python script.

You provide an input CSV file of text data that will be individually sent to local llama as context to generate prompt-completion pairs from. This means that there is minimized hallucination since we feed directly in-context into llama the information we want it to write prompt-completion pairs about!

## Uses

### Prepare Your Data:

Write your custom text data in chunk.csv. Each row in this CSV file will be treated as an individual input to the LLaMA model.
Generate Prompt-Completion Pairs:

Run the main.py script. This script will process the data in chunk.csv and generate prompt-completion pairs using LLaMA. The generated data will be saved in output_alpaca.json in the Alpaca format.

### Fine-Tuning:
1. If you don't have a powerful GPU, you can use Google Colab to fine-tune your model. Open the Google Colab link. https://colab.research.google.com/drive/1eRTPn37ltBbYsISy9Aw2NuI2Aq5CQrD9?usp=sharing

2. Run the first block of code. It will create/clone LLaMA-Factory directory 

3. In the Colab file manager, navigate to the LLaMA-Factory/data directory and place your output_alpaca.json file there.

4. Update the identity.json file in the same directory.(LLaMA-Factory/data)

5. include the path to your output_alpaca.json to identity.json file . Your will find this file in same directory to include the path to your output_alpaca.json file. For example: An example entry in identity.json would look like this:
json
```
{
  "identity": {
    "file_name": "identity.json"
  },
  "alpaca_en_demo": {
    "file_name": "alpaca_en_demo.json"
  },
  "output_alpaca.json": {
    "file_name": "output_alpaca.json"
  },
  "alpaca_zh_demo": {
    "file_name": "alpaca_zh_demo.json"
  }
  // ... other configurations
}
```
6. Run the remaining cells in the Colab notebook:

Continue running the rest of the cells to complete the fine-tuning process.
You may skip the step for "**Fine-tune model via LLaMA Board**" if you do not require a web interface.
By following these instructions, you can generate and fine-tune a custom dataset for your large language model.

Run the remaining cells in the Colab notebook to complete the fine-tuning process. You can skip the "Fine-tune model via LLaMA Board" section if you do not require a web interface.