import csv
import json

def csv_to_alpaca_json(csv_file_path):
    json_data = []
    with open(csv_file_path, newline='') as csvfile:
        csv_reader = csv.DictReader(csvfile)
        for row in csv_reader:
            json_data.append({
                "instruction": row['Question'],
                "output": row['Answer']
            })
    return json_data

csv_file_path = 'output.csv'  # Update with your CSV file path
json_data = csv_to_alpaca_json(csv_file_path)

# Adding optional fields (input, system, history) is not necessary for your dataset
# If you want to include those, modify the code accordingly

with open('output_alpaca.json', 'w') as jsonfile:
    json.dump(json_data, jsonfile, indent=2)
''' https://github.com/hiyouga/LLaMA-Factory/tree/main/data


Currently we support dataset in alpaca or sharegpt format, the dataset in alpaca format should follow the below format:

[
  {
    "instruction": "user instruction (required)",
    "input": "user input (optional)",
    "output": "model response (required)",
    "system": "system prompt (optional)",
    "history": [
      ["user instruction in the first round (optional)", "model response in the first round (optional)"],
      ["user instruction in the second round (optional)", "model response in the second round (optional)"]
    ]
  }
]
Regarding the above dataset, the description in dataset_info.json should be:

"dataset_name": {
  "file_name": "data.json",
  "columns": {
    "prompt": "instruction",
    "query": "input",
    "response": "output",
    "system": "system",
    "history": "history"
  }
}
The query column will be concatenated with the prompt column and used as the user prompt, then the user prompt would be prompt\nquery. The response column represents the model response.

The system column will be used as the system prompt. The history column is a list consisting string tuples representing prompt-response pairs in the history. Note that the responses in the history will also be used for training in supervised fine-tuning.

For the pre-training datasets, only the prompt column will be used for training, for example:

[
  {"text": "document"},
  {"text": "document"}
]
Regarding the above dataset, the description in dataset_info.json should be:

"dataset_name": {
  "file_name": "data.json",
  "columns": {
    "prompt": "text"
  }
}
For the preference datasets, the response column should be a string list whose length is 2, with the preferred answers appearing first, for example:

[
  {
    "instruction": "user instruction",
    "input": "user input",
    "output": [
      "chosen answer",
      "rejected answer"
    ]
  }
]
Regarding the above dataset, the description in dataset_info.json should be:

"dataset_name": {
  "file_name": "data.json",
  "ranking": true,
  "columns": {
    "prompt": "instruction",
    "query": "input",
    "response": "output",
  }
}
'''
