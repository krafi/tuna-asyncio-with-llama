import os
import json
import pandas as pd
import asyncio
from itertools import combinations
from tqdm import tqdm
import ollama

MODEL = 'llama3'

class TokenBucket:
    """
    This class implements the token bucket algorithm which helps in rate limiting.
    The idea is that tokens are added to the bucket at a fixed rate and consumed when requests are made.
    If the bucket is empty, it means the rate limit has been exceeded.
    """

    def __init__(self, rate: int, capacity: int):
        """
        Initialize a new instance of TokenBucket.

        Parameters:
        - rate: The rate at which tokens are added to the bucket.
        - capacity: Maximum number of tokens the bucket can hold.
        """
        self._rate = rate
        self._capacity = capacity
        self._tokens = capacity
        self._last_refill = asyncio.get_event_loop().time()

    async def consume(self):
        """
        Consume a token from the bucket. If no tokens are available, it waits.
        """
        while not self._consume():
            await asyncio.sleep(0)
        await asyncio.sleep(1 / self._rate)

    def _consume(self):
        """
        Internal method to consume a token. Refills the bucket based on elapsed time.
        """
        current_time = asyncio.get_event_loop().time()
        elapsed_time = current_time - self._last_refill

        refill_tokens = self._rate * elapsed_time
        self._tokens = min(self._capacity, self._tokens + refill_tokens)
        self._last_refill = current_time

        if self._tokens >= 1:
            self._tokens -= 1
            return True
        return False

# Semaphore is used to limit the number of simultaneous requests.
SEMAPHORE = asyncio.Semaphore(80)
RATE_LIMITER = TokenBucket(80, 80)  # Set rate and capacity both to 80.

async def generate_response_async(chunks, num_triplets, model_choice, custom_system_message, custom_prompt_prefix, custom_json_format, debug_file):
    """
    Asynchronous function to generate a response from the Llama3 model.

    Parameters:
    - chunks: List of text chunks to consider.
    - num_triplets: Number of questions and answers to generate.
    - model_choice: The model choice for the Llama3 model.
    - custom_system_message: Custom system message for the prompt.
    - custom_prompt_prefix: Custom prefix for the prompt.
    - custom_json_format: Desired format of the response JSON.
    - debug_file: File path to write debug information.
    """
    # Ensure we are within rate limits
    await RATE_LIMITER.consume()

    async with SEMAPHORE:
        combined_text = "\n\n".join([
            f"Text {chunk['ChunkID']} {{\n{chunk['ChunkText']}\n}}"
            for chunk in chunks
        ])

        # Construct the prompt for the Llama3 model
        prompt = custom_prompt_prefix + combined_text + "\n\nThe structure of your JSON response should be:\n[" + ",\n".join(
            [custom_json_format for _ in range(num_triplets)]) + "\n]" +  "Keep remember you are a json return program. Ensure the JSON format is correct and well-structured. The response should only contain the JSON data without any additional text like 'Here is the JSON response:  or 'Note' at the end avoid."  
        # 

        # Construct the payload for the Llama3 model
        payload = {
            "model": model_choice,
            "messages": [{
                "role": "system",
                "content": custom_system_message
            }, {
                "role": "user",
                "content": prompt
            }],
            "temperature": 0.0
        }

        # Log input to AI
        with open(debug_file, 'a') as f:
            f.write("Input to AI:\n")
            f.write(json.dumps(payload, indent=4))
            f.write("\n\n")

        # Llama3 model request
        response = ollama.chat(model=model_choice, messages=payload['messages'])

        # Log output from AI
        with open(debug_file, 'a') as f:
            f.write("Output from AI:\n")
            f.write(json.dumps(response, indent=4))
            f.write("\n\n")

  
        

        try:
            # Find the starting index of the JSON part
            start_index = response.find("[")
            # Find the ending index of the JSON part
            end_index = response.rfind("]") + 1

            # Extract the JSON part
            response = response[start_index:end_index]

            
        except Exception as e:
            print("\033[91m JSON response: not match with [] , But its ok! lets try with {}", "\033[0m")

            try:
                # Find the starting index of the JSON part
                start_index = response.find("{")
                # Find the ending index of the JSON part
                end_index = response.rfind("}") + 1

                # Extract the JSON part
                response = response[start_index:end_index]
                print(json.dumps(response, indent=4))
            except Exception as e:
                print(json.dumps(response, indent=4))
                print("\033[91m JSON response: not with {} maybe thats a problem","lets try with other method", "\033[0m")
                pass
            

        # Extract questions and answers from response
        questions = []
        try:
            content = response['message']['content']
            data = json.loads(content)
            for triplet in data:
                question = triplet['question']
                answer = triplet['answer']
                quoted_text_id = triplet['quoted_text']
                questions.append([question, answer, quoted_text_id])
        except (KeyError, json.JSONDecodeError) as e:
            print("\033[91mError 2: We need to skip this. parsing JSON response:", e, "\033[0m")
            return []

        return questions

async def main(input_csv, debug_file):
    """
    Main function to orchestrate the process.

    Parameters:
    - input_csv: Path to the input CSV file containing text chunks.
    - debug_file: File path to write debug information.
    """
    # Read the CSV file
    df = pd.read_csv(input_csv, encoding='utf-8')
    questions = []

    # Custom messages and formats
    custom_system_message = "You are an expert in splitting and  Custom Dataset generator program. You only know to replay in JSON"
    num_triplets = 6
    custom_json_format = '{start with [ and finish with ] pure json, "question": "insert question", "answer":"insert answer", "quoted_text": "insert the number of the text that was used to answer the question"}'
    custom_prompt_prefix = (f"Your task is to create {num_triplets} sets of questions and sarcastic, intelligent sense of humor answers based on the provided texts.  "
                            "The answers should be humorous and end with exclamation points. "
                            "Output the results as a JSON list containing the questions, their corresponding answers, and an indication of which text each answer is derived from."
                            "Provide a JSON list of {num_triplets} questions and their answers, along with an indication of which text the answer was derived from, from the following texts:\n")

    model_choice = 'llama3'
    num_chunks = 3

    # Create all possible combinations of chunks
    chunk_combinations = list(combinations(df.iterrows(), num_chunks))

    # Initialize a progress bar
    pbar = tqdm(total=len(chunk_combinations), desc="Processing chunks", position=0, leave=True)

    async def wrapped_generate_response_async(chunk_group):
        """Helper function to wrap the generation and update progress bar."""
        result = await generate_response_async([row[1] for row in chunk_group], num_triplets, model_choice, custom_system_message, custom_prompt_prefix, custom_json_format, debug_file)
        pbar.update(1)
        return result

    # Create a list of tasks for all chunk combinations
    tasks = [
        wrapped_generate_response_async(chunk_group)
        for chunk_group in chunk_combinations
    ]

    # Execute all tasks and wait for their completion
    results = await asyncio.gather(*tasks)

    pbar.close()

    # Process results and append to questions list
    for triplet_group, chunk_group in zip(results, chunk_combinations):
        for triplet in triplet_group:
            if isinstance(triplet, list) and len(triplet) >= 3:
                question, answer, quoted_text_id = triplet
                chunk_ids = ", ".join(str(row[1]['ChunkID']) for row in chunk_group)
                questions.append([chunk_ids, question, answer, quoted_text_id])
            else:
                questions.append(["NA", "NA", "NA", "NA"])
                
    # Convert questions list to DataFrame and save to CSV
    question_df = pd.DataFrame(questions,
                               columns=['ChunkIDs', 'Question', 'Answer', 'Quoted_Text_ID'])
    
    # Save DataFrame to CSV
    question_df.to_csv('output.csv', index=False)

# If script is run as main, execute the main function
if __name__ == '__main__':
    # Set debug file path
    debug_file = 'debug_log.txt'
    # Run main function
    asyncio.run(main("chunks.csv", debug_file))
